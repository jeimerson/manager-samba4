#!/bin/bash

# Função para criar um novo usuário
criar_usuario() {
  nome=$(dialog --inputbox "Digite o nome do usuário:" 8 40 --stdout)
  smbpasswd -a $nome
  dialog --msgbox "Usuário criado com sucesso!" 8 40
}

# Função para listar usuários existentes
listar_usuarios() {
  usuarios=$(pdbedit -L | awk -F: '{print $1}')
  dialog --textbox <(echo "$usuarios") 20 60
}

# Função para excluir um usuário existente
excluir_usuario() {
  usuario=$(dialog --inputbox "Digite o nome do usuário a ser excluído:" 8 40 --stdout)
  smbpasswd -x $usuario
  dialog --msgbox "Usuário excluído com sucesso!" 8 40
}

# Função para criar um novo grupo
criar_grupo() {
  nome=$(dialog --inputbox "Digite o nome do grupo:" 8 40 --stdout)
  groupadd $nome
  dialog --msgbox "Grupo criado com sucesso!" 8 40
}

# Função para listar grupos existentes
listar_grupos() {
  grupos=$(cat /etc/group | cut -d: -f1)
  dialog --textbox <(echo "$grupos") 20 60
}

# Função para excluir um grupo existente
excluir_grupo() {
  grupo=$(dialog --inputbox "Digite o nome do grupo a ser excluído:" 8 40 --stdout)
  groupdel $grupo
  dialog --msgbox "Grupo excluído com sucesso!" 8 40
}

# Função para criar um novo compartilhamento
criar_compartilhamento() {
  nome=$(dialog --inputbox "Digite o nome do compartilhamento:" 8 40 --stdout)
  path=$(dialog --inputbox "Digite o caminho para o compartilhamento:" 8 40 --stdout)
  writeable=$(dialog --inputbox "O compartilhamento é gravável? (yes ou no)" 8 40 --stdout)
  echo "[$nome]" >> /etc/samba/smb.conf
  echo "path = $path" >> /etc/samba/smb.conf
  echo "writeable = $writeable" >> /etc/samba/smb.conf
  systemctl restart smb
  dialog --msgbox "Compartilhamento criado com sucesso!" 8 40
}

# Função para listar compartilhamentos existentes
listar_compartilhamentos() {
  compartilhamentos=$(grep "\[" /etc/samba/smb.conf | awk -F\] '{print $1}' | awk -F\[ '{print $2}')
  dialog --textbox <(echo "$compartilhamentos") 20 

}

# Função para excluir um compartilhamento existente
excluir_compartilhamento() {
  compartilhamento=$(dialog --inputbox "Digite o nome do compartilhamento a ser excluído:" 8 40 --stdout)
  sed -i "/\[$compartilhamento\]/,/path/ d" /etc/samba/smb.conf
  systemctl restart smb
  dialog --msgbox "Compartilhamento excluído com sucesso!" 8 40
}

# Menu principal
while true; do
  opcao=$(dialog --menu "Gerenciador do Samba4" 0 0 0 \
          1 "Criar usuário" \
          2 "Listar usuários" \
          3 "Excluir usuário" \
          4 "Criar grupo" \
          5 "Listar grupos" \
          6 "Excluir grupo" \
          7 "Criar compartilhamento" \
          8 "Listar compartilhamentos" \
          9 "Excluir compartilhamento" \
          10 "Sair" --stdout)
  case $opcao in
    1) criar_usuario;;
    2) listar_usuarios;;
    3) excluir_usuario;;
    4) criar_grupo;;
    5) listar_grupos;;
    6) excluir_grupo;;
    7) criar_compartilhamento;;
    8) listar_compartilhamentos;;
    9) excluir_compartilhamento;;
    10) exit;;
  esac
done

